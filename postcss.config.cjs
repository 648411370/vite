// module.exports = {
//   plugins: {
//     'postcss-pxtorem': {
//       rootValue: 37.5,
//       // 根字体大小是 37.5
//       propList: ['*'],
//       selectorBlackList: ['.no__rem']
//       // 过滤掉.no__rem-开头的class，不进行rem转换处理
//     }
//   }
// }

module.exports = {
  plugins: {
    // postcss-pxtorem 插件的版本需要 >= 5.0.0
    // 适用版本 5.1.1
    // yarn add -D postcss-pxtorem@5.1.1
    // npm install postcss-pxtorem@5.1.1 -D
    'postcss-pxtorem': {
      // rootValue({ file }) { // 判断是否是vant的文件 如果是就使用 37.5为根节点字体大小
      //     // 否则使用75 因为vant使用的设计标准为375 但是市场现在的主流设置尺寸是750
      //     return file.indexOf('vant') !== -1 ? 37.5 : 75;
      // },
      rootValue: 37.5,
      // 配置哪些文件中的尺寸需要转化为rem *表示所有的都要转化
      propList: ['*'],
    },
  },
};