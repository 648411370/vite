// import { createApp } from 'vue'
// import './style.css'
// import App from './App.vue'

// createApp(App).mount('#app')

import { createApp } from "vue";
import { Button, Tabbar, TabbarItem, Toast, ActionSheet } from "vant";
import "vant/lib/index.css";
import App from "./App.vue";

import router from "./router"; // 引入router
// 引入 Pinia 状态管理工具
import pinia from "./store";
// import './untils/rem'
import "amfe-flexible";
// 国际化
import i18n from "./locals";
router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  if (typeof to.meta.title === "string") {
    document.title = to.meta?.title;
  }
  if (to.path == "/login") {
    next();
  } else {
    let { token } = JSON.parse(localStorage.getItem("user") || "0");
    if (token) {
      next();
    } else {
      router.push("/");
    }
  }
});
const app = createApp(App);
const theme = {
  color: "red",
};
app.config.globalProperties.$theme = theme;

app
  .use(pinia)
  .use(Button)
  .use(Tabbar)
  .use(TabbarItem)
  .use(Toast)
  .use(ActionSheet)
  .use(i18n)
  .use(router)
  .mount("#app");
