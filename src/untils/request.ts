import { Toast } from "vant";
import axios, { AxiosResponse, AxiosRequestConfig } from "axios";
const isDev = process.env.NODE_ENV == "development";
console.log('isDev', isDev)
const request = axios.create({
  // baseURL: isDev ? "https://v3.api.fufentong.com" : "",
  baseURL: import.meta.env.VITE_BASE_URL,
  timeout: 30000,
  headers: {
    timestamp: new Date().getTime(),
    "Content-Type": "application/json;chareset=UTF-8",
  },
});
//请求拦截器
request.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    if (config.headers) {
      //   config.headers.token = sessionStorage.getItem("token") || "";
      config.headers["login-token"] =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJmZnQiLCJpYXQiOjE2NjA2MzQyMzIsImV4cCI6MTY2MzIyNjIzMiwiaXAiOiIxOTIuMTg4LjExLjEyMCIsImlkIjo5NTUzLCJtb2JpbGUiOiIxNzgzMDczNTgyMSIsIm1lbWJlcl9uYW1lIjoi6LCt56uL5b-XIiwiYWNjb3VudCI6IjE3ODMwNzM1ODIxIiwiYXZhdGFyIjoiXC91cGxvYWRcL2ltYWdlXC8yMDIyMDMxNVwvOTE0ZmRkMDMwMTYxMTE3N2Q0ZWFhMWY2OTNlMWY1NTAucG5nIiwiYnVzaW5lc3NfaWQiOjEzLCJkZXBhcnRtZW50X2lkIjo1LCJuYW1lIjoi5rWL6K-V5LiT55So5LyB5LiaIiwiY2hhbm5lbF9pZCI6MTQsInNlbGVjdGVkIjoxLCJidXNpbmVzc19zdGF0dXMiOjEsInNob3BfbmFtZSI6IuWVhuWfjjExMTExMTExIiwiY29sb3IiOiIjRkYzQjNCIiwibG9nbyI6Imh0dHA6XC9cL2ltZy5mdWZlbnRvbmcuY29tXC91cGxvYWRcL2ltYWdlXC8yMDIyMDcyMFwvYWZkMTIzY2UyNmYzNThlZDJkYTM5M2Y3NGM5MmMxYjguanBnIn0.qVTtka2Q59nGaE8V8oG3xS_rotpRrg-MPHnAHcB-_yo";
      config.headers["shop-token"] =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJmZnQiLCJpYXQiOjE2NjA2Mzc2NjQsImV4cCI6MTY2MzIyOTY2NCwiaXAiOiIxOTIuMTg4LjExLjEyMCIsImlkIjo5NTUzLCJtb2JpbGUiOiIxNzgzMDczNTgyMSIsImFjY291bnQiOiIxNzgzMDczNTgyMSIsImJ1c2luZXNzX2lkIjoxMywiZGVwYXJ0bWVudF9pZCI6NSwic2hvcF9pZCI6MjM2fQ.Aihkk69dDVSOsnB3K119ShpdwODII0f04KSm0DC_UWA";
    }
    return config;
  },
  (error: any) => {
    return Promise.reject(error);
  }
);
//响应拦截器
request.interceptors.response.use(
  (response: AxiosResponse) => {
    // const code = response.data.code;
    const { code, message } = response.data;
    if (code != 200) {
      Toast(message);
    }
    return response.data;
  },
  (error: any) => {
    return Promise.reject(error);
  }
);
export default request;
