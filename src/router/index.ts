
/**
  ts版本的路由
  2022年8月17日21:00:46
 */

import {
  createRouter,
  createWebHashHistory,
  createWebHistory,
  RouteRecordRaw,
} from "vue-router";

// import home from "@/pages/home/index.vue";
import Layout from "../layout/index.vue";
const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/login",
    children: [
      {
        path: "/login",
        name: "Login",
        meta: { title: "登录", KeepAlive: false },
        component: () => import("../pages/login/index.vue"),
      },
    ],
  },

  {
    path: "/homes",
    component: Layout,
    redirect: "/home",
    children: [
      {
        path: "/home",
        name: "Home",
        meta: { title: "测试1", KeepAlive: false },
        component: () => import("../pages/home/index.vue"),
      },
      {
        path: "/classify",
        name: "Classify",
        meta: { title: "测试2", KeepAlive: false },
        component: () => import("../pages/classify/index.vue"),
      },
      {
        path: "/cart",
        name: "Cart",
        meta: { title: "测试3", KeepAlive: true },
        component: () => import("../pages/cart/index.vue"),
      },
      {
        path: "/my",
        name: "My",
        meta: { title: "测试4", KeepAlive: false },
        component: () => import("../pages/my/index.vue"),
      },
    ],
  },

  {
    path:'/p5',
    name:'p5',
    component:()=> import('../pages/text/p5.vue')
  },
  {
    path:'/json',
    name:'json',
    component:()=> import('../pages/text/json.vue')
  },
  {
    path:'/pinia',
    name:'pinia',
    component:()=> import('../pages/text/pinia.vue')
  }
  
];

/**
 * 定义返回模块
 */
const router = createRouter({
  history: createWebHistory("#"), // 设置为history模式，就是路径里面没有#,  createWebHashHistory 是默认的，括号里面的就是基础路径，可以理解为项目名称，就是请求路径的基础url
  routes,
});

export default router;
