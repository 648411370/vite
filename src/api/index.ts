import request from "../untils/request";

interface Ilist {
  username: string;
  password: string;
}


type Iuser = {
  name:string
}
export const postLogin = (params: Ilist) => {
  return request({
    url: "/login",
    method: "post",
    data: params,
  });
};

export const getuser = (params:Iuser) => {
  return request({
    url: "/user",
    method: "get",
    params,
  });
};
