import { defineStore } from "pinia";

export const testStroe = defineStore("test", {
  state: () => {
    return {
      counter: 0,
    };
  },
  // 开启持久化
  persist: {
    enabled: true, // 启用
    strategies: [
      // storage 可选localStorage或sessionStorage
      // paths 给指定数据持久化
      { key: "testnums", storage: localStorage, paths: ["counter"] },
    ],
  },
  actions: {
    handleAddConter(val: number) {
        console.log('val', val)
      this.counter = val;
    },
  },
});
