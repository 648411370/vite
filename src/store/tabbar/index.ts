import { defineStore } from "pinia";

interface IstringorNumber {
  vals: number;
}

export const userStore = defineStore("tab", {
  state: () => {
    return {
      tabActive: 0,
    };
  },
  // 开启持久化
  persist: {
    enabled: true, // 启用
    strategies: [
      // storage 可选localStorage或sessionStorage
      // paths 给指定数据持久化
      { key: "tabBarBumber", storage: localStorage, paths: ["tabActive"] },
    ],
  },
  getters: {},
  actions: {
    handleChangeTab(val: number) {
      this.tabActive = val;
    },
  },
});
