import { defineStore } from "pinia";

interface IstringorNumber {
  vals: number;
}

type Iprops = {
  token: number;
};
export const userStore = defineStore("main", {
  state: () => {
    return {
      name: "小张11",
      age: 10,
      age1: 11,
      userInfo: {
        username: "1",
        password: "2",
      },
      token: 0,
    };
  },
  // 开启持久化
  persist: {
    enabled: true, // 启用
    strategies: [
      // storage 可选localStorage或sessionStorage 
      // paths 给指定数据持久化
      { key: "user", storage: localStorage, paths: ["token"] },
    ],
  },
  getters: {
    add(state) {
      return (val: number) => state.age + val + this.exit(2);
    },
    exit: (state) => {
      return (val: number) => state.age1 + val;
    },
  },
  actions: {
    handleInfo(val: string) {
      this.name = val;
    },
    handlePinaLogin(val:Iprops){
      this.token = val.token
    }
  },
});
