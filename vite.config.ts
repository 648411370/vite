import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import Components from "unplugin-vue-components/vite";
import { VantResolver } from "unplugin-vue-components/resolvers";
// https://vitejs.dev/config/
export default defineConfig({
  //本地运行配置，以及反向代理配置
  server: {
    hmr: true,
    host: "localhost", //配置network
    strictPort: false, //设为true时端口被占用则直接退出，不会尝试下一个可用端口
  },
  //打包配置
  build: {
    //指定输出路径
    outDir: "dist",
    //生成静态资源的存放路径
    assetsDir: "assets",
  },

  plugins: [
    vue(),
    Components({
      resolvers: [VantResolver()],
    }),
  ],
});
